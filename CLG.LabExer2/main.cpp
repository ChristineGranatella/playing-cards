
//Demo 3
//Christine Granatella

#include <iostream>
#include <conio.h>
#include <string>

using namespace std;



enum Rank
{
	Two = 2,
	Three = 3,
	Four = 4,
	Five = 5,
	Six = 6,
	Seven = 7,
	Eight = 8,
	Nine = 9,
	Ten = 10,
	Jack = 11,
	Queen = 12,
	King = 13,
	Ace = 14,
};

enum Suit
{
	Spades,
	Hearts,
	Diamonds,
	Clubs
};

struct Card
{
	Rank Rank;
	Suit Suit;
};

void PrintCard(Card card);
Card HighCard(Card c1, Card c2);

int main()

{
	Card c1;
	c1.Rank = Two;
	c1.Suit = Hearts;
	PrintCard(c1);

	Card c2;
	c2.Rank = Four;
	c2.Suit = Spades;

	PrintCard(HighCard(c1, c2));

	_getch();
	return 0;
}

void PrintCard(Card card)
{
	switch (card.Rank)
	{
	case Two: cout << "The two of "; break;
	case Three: cout << "The three of "; break;
	case Four: cout << "The four of "; break;
	case Five: cout << "The five of "; break;
	case Six: cout << "The six of "; break;
	case Seven: cout << "The seven of "; break;
	case Eight: cout << "The eight of "; break;
	case Nine: cout << "The nine of "; break;
	case Ten: cout << "The ten of "; break;
	case Jack: cout << "The Jack of "; break;
	case Queen: cout << "The Queen of "; break;
	case King: cout << "The King of "; break;
	case Ace: cout << "The Ace of "; break;
	}

	switch (card.Suit)
	{
	case Spades: cout << "Spades\n"; break;
	case Hearts: cout << "Hearts\n"; break;
	case Clubs: cout << "Clubs\n"; break;
	case Diamonds: cout << "Diamonds\n"; break;
	}
}

Card HighCard(Card c1, Card c2)
{
	if (c1.Rank > c2.Rank) return c1;

	return c2;
}





